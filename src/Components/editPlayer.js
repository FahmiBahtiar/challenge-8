import React from "react";
import "./index.css";

export default function editplayer() {
  return (
    <div class="Add Player">
        <h1>Edit Form</h1>
      <form class="form">
        {/* Uncomment the next line to show the success message */}
        {/* <div class="success-message">Success! Thank you for Add Player</div> */}
        <input
          id="Username"
          class="form-field"
          type="text"
          placeholder="username"
          name="username"
        />
        {/* Uncomment the next line to show the error message */}
        {/* <span id="username-error">Please enter a Username</span> */}
        <input
          id="email"
          class="form-field"
          type="text"
          placeholder="Email"
          name="email"
        />
        {/* Uncomment the next line to show the error message */}
        {/* <span id="email-error">Please enter a email </span> */}
        <input
          id="experience"
          class="form-field"
          type="text"
          placeholder="Experience"
          name="experience"
        />
        {/* Uncomment the next line to show the error message */}
        {/* <span id="experience-error">Please enter an Experience</span> */}
        <input
          id="lvl"
          class="form-field"
          type="text"
          placeholder="Level"
          name="lvl"
        />
        {/* Uncomment the next line to show the error message */}
        {/* <span id="lvl-error">Please enter an Level</span> */}
        <button class="form-field" type="submit">
          Add Player
        </button>
      </form>
    </div>
  );
}