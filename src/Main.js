import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Menu from './Components/Menu';
import addPlayer from './Components/addPlayer';
import editplayer from './Components/editPlayer';
import searchplayer from './Components/search'


const Main = () => {
  return (
    <Switch> {/* The Switch decides which component to show based on the current URL.*/}
      <Route exact path='/' component={Menu}></Route>
      <Route exact path='/addplayer' component={addPlayer}></Route>
      <Route exact path='/editplayer' component={editplayer}></Route>
      <Route exact path='/searchplayer' component={searchplayer}></Route>



    </Switch>
  );
}

export default Main;